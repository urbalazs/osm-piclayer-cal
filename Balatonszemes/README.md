# Balatonszemes

Balatonszemes község Képviselő-testületének [12/2010.(V.17.) önkormányzati rendelete](https://or.njt.hu/onkormanyzati-rendelet/549326) Balatonszemes község helyi építési szabályzatáról szól. A rendelet mellékletei tartalmazzák az utakat, a telekhatárokat, az épületeket és a házszámokat is. Ha be szeretnéd tölteni a JOSM-be, akkor futtasd le a következő parancsokat.

Töltsd le a belterületi és külterületi szabályozási terveket. Körülbelül 10,2 MB kerül letöltésre.

```
wget "https://or.njt.hu/download/2460/resources/EJR_14664569-1._mell_klet_15-2021__XI.10.__r-el_m_dos_tva.pdf" -O SZ01-202111.pdf
wget "https://or.njt.hu/download/2460/resources/EJR_14664583-2.mell_klet_10-2016__V.26.__r-el_m_dos_tva.pdf" -O SZ02-201606.pdf
wget "https://or.njt.hu/download/2460/resources/EJR_14664587-3._mell_klet_15-2021__XI.10.__r-el_m_dos_tva.pdf" -O SZ03-202111.pdf
wget "https://or.njt.hu/download/2460/resources/EJR_14664589-4._mell_klet_18-2019__XII.17.__r-el_m_dos_tva.pdf" -O SZ04-201912.pdf
wget "https://or.njt.hu/download/2460/resources/EJR_14664592-5.mell_klete_17-2016__XII.12.__r-el_m_dos_tva.pdf" -O SZ05-201612.pdf
wget "https://or.njt.hu/download/2460/resources/EJR_14664593-6.mell_klete_17-2016__XII.12.__r-el_m_dos_tva.pdf" -O SZ06-201612.pdf
wget "https://or.njt.hu/download/2460/resources/EJR_14665210-7._mell_klet_Jelmagyar_zat_13-2020__VIII.10.__r-el_m_dos_tva.pdf" -O Szelvenyezes.pdf
wget "https://or.njt.hu/download/2460/resources/EJR_14665219-8._mell_klet_2-2020__II.17.__r-el_m_dos_tva.pdf" -O Kulterulet-202002.pdf
wget "https://or.njt.hu/download/2460/resources/EJR_14665234-9.melleklet_17-2016__XII.12.__r-el_m_dos_tva.pdf" -O KM09-201612.pdf
```

Alakítsd át a PDF-fájlokat JPG-fájlokká. Az eredményül kapott képek mérete körülbelül 26,3 MB.

```
gs -sDEVICE=jpeg -r150 -o SZ01-202111.jpg SZ01-202111.pdf
gs -sDEVICE=jpeg -r150 -o SZ02-201606.jpg SZ02-201606.pdf
gs -sDEVICE=jpeg -r150 -o SZ03-202111.jpg SZ03-202111.pdf
gs -sDEVICE=jpeg -r150 -o SZ04-201912.jpg SZ04-201912.pdf
gs -sDEVICE=jpeg -r150 -o SZ05-201612.jpg SZ05-201612.pdf
gs -sDEVICE=jpeg -r150 -o SZ06-201612.jpg SZ06-201612.pdf
gs -sDEVICE=jpeg -r300 -o Kulterulet-202002.jpg Kulterulet-202002.pdf
gs -sDEVICE=jpeg -r150 -o KM09-201612.jpg KM09-201612.pdf
```

Nyisd meg a képfájlokat a JOSM szerkesztővel a *Légi felvétel* → *Új kép réteg fájlból…* menüpont használatával.

---


## Dr. Kiss Ödön Orvosi Rendelő

Balatonszemes honlapjára fel lett töltve az orvosi rendelő [alaprajza](https://www.balatonszemes.hu/wp-content/uploads/images/alaprajz1.jpg).

Az alaprajz a következő parancs használatával tölthető le. A fájl mérete körülbelül 2,9 MB.

```
wget "https://www.balatonszemes.hu/wp-content/uploads/images/alaprajz1.jpg" -O orvosi-rendelo.jpg
```


## Berzsenyi utcai szabadstrand

Balatonszemes honlapján a Berzsenyi utcai szabadstrand fejlesztéséről szóló [hír](https://www.balatonszemes.hu/1355-2/) mellékletében található egy 1:500 méretű [helyszínrajz](https://www.balatonszemes.hu/wp-content/uploads/2020/10/Balatonszemes-strand-E%CC%81PU%CC%88LET_E%CC%81-0-Helyszi%CC%81nrajz-M1_500.pdf).

Töltsd le a hírben lévő helyszínrajzot. A fájl mérete körülbelül 830 kB.

```
wget "https://www.balatonszemes.hu/wp-content/uploads/2020/10/Balatonszemes-strand-E%CC%81PU%CC%88LET_E%CC%81-0-Helyszi%CC%81nrajz-M1_500.pdf" -O berzsenyi-strand.pdf
```

Alakítsd át a PDF-fájlt JPG-fájllá. Az eredményül kapott kép mérete körülbelül 1,8 MB.

```
gs -sDEVICE=jpeg -r150 -o berzsenyi-strand.jpg berzsenyi-strand.pdf
```
