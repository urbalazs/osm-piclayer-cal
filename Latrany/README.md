# Látrány

Látrány község képviselő-testületének [7/2004. (IV.15.) önkormányzati rendelete](https://or.njt.hu/eli/v01/396156/r/2004/7) Látrány község helyi építési szabályozásáról. A rendelet mellékletei tartalmazzák az utakat, a telekhatárokat, az épületeket és a házszámokat is. Ha be szeretnéd tölteni a JOSM-be, akkor futtasd le a következő parancsokat.

Töltsd le a belterületi és külterületi szabályozási terveket. Körülbelül 17,1 MB kerül letöltésre.

```
wget "https://or.njt.hu/download/2585/resources/EJR_42568359-7-592560belter_leti.pdf" -O Belterulet.pdf
wget "https://or.njt.hu/download/2585/resources/EJR_42899778-K_lter_szab.pdf" -O Kulterulet.pdf
```

Alakítsd át a PDF-fájlokat JPG-fájlokká. Az eredményül kapott képek mérete körülbelül 9,4 MB.

```
gs -sDEVICE=jpeg -r150 -o Belterulet.jpg Belterulet.pdf
gs -sDEVICE=jpeg -r150 -o Kulterulet.jpg Kulterulet.pdf
```

Nyisd meg a képfájlokat a JOSM szerkesztővel a *Légi felvétel* → *Új kép réteg fájlból…* menüpont használatával.
