# Budapest XIII. kerület

# Térképet tartalmazó hírek

A következő weboldalak és hírek térképmásolatot, térképrajzot, látványterveket, alaprajzokat és helyszínrajzokat is tartalmaznak. A letöltésük után a fent ismertetett módon lehet megnyitni azokat a JOSM szerkesztővel.


## Thurzó park

A XIII. kerületi Közszolgáltató Zrt. honalpján a Thurzó parkról szóló [hírben](https://kozszolgaltato.bp13.hu/hirek/thurzo-park/) elérhető a park terve. További részletes dokumentumok pedig a [Google Drive-on](https://drive.google.com/drive/folders/1uQDrZufovHJSb-wBO-0AD2ZH1UaIo3Dk?fbclid=IwAR13UHgDnG0To-jw2JelgqyldLePhgrv64NkGbjy7ROWICXFej9mGnh8ycw) találhatók.

A közpark terve közvetlenül a hírből vagy a következő parancs használatával tölthető le. A fájl mérete körülbelül 771 KB.

```
wget https://kozszolgaltato.bp13.hu/wp-content/uploads/2022/12/Thurzo_Park_Kiviteli_terv_alaprajz_burkolatjelekkel_221201_1.jpg -O thurzo-park.jpg
```

